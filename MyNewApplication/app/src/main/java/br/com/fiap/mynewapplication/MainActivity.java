package br.com.fiap.mynewapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lista;
    private ArrayList<Funcionario> funcionarios;
    private FuncionarioAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = findViewById(R.id.lista);

        funcionarios = new ArrayList<Funcionario>();
        funcionarios.add(new Funcionario("João Victor", "Software Developer"));
        funcionarios.add(new Funcionario("Elzebio", "Engineer"));
        funcionarios.add(new Funcionario("Alfredo", "Database Administrator"));
        funcionarios.add(new Funcionario("Antonio", "Network Administrator"));

        adapter = new FuncionarioAdapter(MainActivity.this, funcionarios);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Funcionario func = funcionarios.get(position);

                Toast.makeText(MainActivity.this, "Esse é o " + func, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
