package br.com.fiap.mynewapplication;

import java.io.Serializable;

/*
 * Não seguimos o padrão de pacotes utilizado em projetos MVC - Este projeto é apenas teste
 *
 */
public class Funcionario implements Serializable {

    private String nome;
    private String cargo;

    public Funcionario() {
    }

    public Funcionario(String nome, String cargo) {
        this.nome = nome;
        this.cargo = cargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
