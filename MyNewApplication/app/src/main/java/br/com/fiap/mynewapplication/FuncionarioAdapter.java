package br.com.fiap.mynewapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FuncionarioAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Funcionario> funcionarios;

    public FuncionarioAdapter(Context context, ArrayList<Funcionario> funcionarios) {
        this.context = context;
        this.funcionarios = funcionarios;
    }

    @Override
    public int getCount() {
        return funcionarios.size();
    }

    @Override
    public Object getItem(int position) {
        return funcionarios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View inflater = LayoutInflater.from(context).inflate(R.layout.item, parent, false);

        TextView nome = inflater.findViewById(R.id.nome);
        TextView cargo = inflater.findViewById(R.id.cargo);
        Funcionario func = (Funcionario) getItem(position);

        nome.setText(func.getNome());
        cargo.setText(func.getCargo());

        return inflater;
    }
}
