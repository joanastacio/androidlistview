package br.com.fiap.appcadastrp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import br.com.fiap.manipulandoimagens.R;

public class MainActivity extends AppCompatActivity {

    private EditText nome;
    private EditText endereco;
    private Button cadastrar;
    private Button listar;
    private ArrayList<Cadastro> cadastros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nome = findViewById(R.id.nome);
        endereco = findViewById(R.id.endereco);
        cadastrar = findViewById(R.id.cadastrar);
        listar = findViewById(R.id.listar);
        cadastros = new ArrayList<Cadastro>();

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtNome = nome.getText().toString();
                String txtEndereco = endereco.getText().toString();

                Cadastro cad = new Cadastro(txtNome, txtEndereco);
                cadastros.add(cad);

                nome.setText("");
                endereco.setText("");
            }
        });

        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Redirecionamento de telas
                Intent intent = new Intent(MainActivity.this, ListagemActivity.class);
                intent.putExtra("listagem", cadastros);
                startActivity(intent);
            }
        });
    }
}
