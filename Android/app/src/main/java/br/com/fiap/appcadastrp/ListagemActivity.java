package br.com.fiap.appcadastrp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import br.com.fiap.manipulandoimagens.R;

public class ListagemActivity extends AppCompatActivity {

    private ListView lista;
    private ArrayList<Cadastro> cadastros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        lista = findViewById(R.id.lista);
        cadastros = new ArrayList<Cadastro>();

        // Recebendo os valores enviados pelo Extras no MainActivity
        cadastros = (ArrayList<Cadastro>) getIntent().getExtras().get("listagem");

        CadastroAdapter adapter = new CadastroAdapter(ListagemActivity.this, cadastros);
        lista.setAdapter(adapter);
    }
}
