package br.com.fiap.appcadastrp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.fiap.manipulandoimagens.R;

public class CadastroAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<Cadastro> cadastros;

    public CadastroAdapter(Context context, ArrayList<Cadastro> cadastros) {
        this.context = context;
        this.cadastros = cadastros;
    }

    @Override
    public int getCount() {
        return cadastros.size();
    }

    @Override
    public Object getItem(int position) {
        return cadastros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View tela = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        TextView nome = tela.findViewById(R.id.nome);
        TextView endereco = tela.findViewById(R.id.endereco);

        Cadastro cad = cadastros.get(position);
        nome.setText(cad.getNome());
        endereco.setText(cad.getEndereco());

        return tela;
    }
}
